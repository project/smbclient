<?php

namespace Drupal\smbclient;

/**
 * Provides an interface for a share.
 */
interface SmbclientShareInterface {

  /**
   * Downloads a file.
   *
   * @param string $source
   *   The source path.
   * @param string $target
   *   The target path.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function getFile($source, $target);

  /**
   * Uploads a file.
   *
   * @param string $source
   *   The source path.
   * @param string $target
   *   The target path.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function putFile($source, $target);

  /**
   * Opens a readable stream to a file.
   *
   * @param string $source
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function readFile($source);

  /**
   * Opens a writable stream to a file.
   *
   * @param string $target
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function writeFile($target);

  /**
   * Opens a writable stream to a file and set the cursor to the end of the file.
   *
   * @param string $target
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function appendFile($target);

  /**
   * Renames a file.
   *
   * @param string $from
   * @param string $to
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function renameFile($from, $to);

  /**
   * Deletes a file.
   *
   * @param string $path
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function deleteFile($path);

  /**
   * Gets a list of files from a dir.
   *
   * @param string $path
   *
   * @return \Icewind\SMB\IFileInfo[]
   *
   * @throws \Exception
   */
  public function getDirInfo($path);

  /**
   * Gets a file info.
   *
   * @param string $path
   *
   * @return \Icewind\SMB\IFileInfo
   *
   * @throws \Exception
   */
  public function getFileInfo($path);

  /**
   * Creates a dir.
   *
   * @param string $path
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function createDir($path);

  /**
   * Deletes a dir.
   *
   * @param string $path
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function deleteDir($path);

}
