<?php

namespace Drupal\smbclient\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\smbclient\SmbclientServerEntityInterface;

/**
 * Class for handling smbclient servers.
 *
 * @ConfigEntityType(
 *   id = "smbclient_server",
 *   label = @Translation("Smbclient Server"),
 *   handlers = {
 *     "access" = "Drupal\smbclient\SmbclientServerAccessControlHandler",
 *     "list_builder" = "Drupal\smbclient\SmbclientServerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smbclient\Form\SmbclientServerAddForm",
 *       "edit" = "Drupal\smbclient\Form\SmbclientServerForm",
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *       "disable" = "Drupal\smbclient\Form\SmbclientServerDisableForm",
 *       "enable" = "Drupal\smbclient\Form\SmbclientServerEnableForm",
 *     }
 *   },
 *   config_prefix = "server",
 *   admin_permission = "administer smbclient",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "title",
 *     "id",
 *     "status",
 *     "weight",
 *     "host",
 *     "auth",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/smbclient/servers/manage/{smbclient_server}",
 *     "delete-form" = "/admin/config/system/smbclient/servers/manage/{smbclient_server}/delete",
 *     "collection" = "/admin/config/system/smbclient/servers",
 *     "disable" = "/admin/config/system/smbclient/servers/manage/{smbclient_server}/disable",
 *     "enable" = "/admin/config/system/smbclient/servers/manage/{smbclient_server}/enable",
 *   }
 * )
 */
class SmbclientServer extends ConfigEntityBase implements SmbclientServerEntityInterface {

  /**
   * The server title.
   *
   * @var string
   */
  protected $title;

  /**
   * The server host.
   *
   * @var string
   */
  protected $host = '';

  /**
   * The plugins.
   *
   * @var \Drupal\smbclient\Plugin\SmbclientServerPlugin[]
   */
  protected $plugins = [];

  /**
   * The auth plugin id.
   *
   * @var array
   */
  protected $auth = ['id' => 'basic'];

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function getHost() {
    return $this->host;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthId() {
    return $this->auth['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHost($value) {
    $this->set('host', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin($plugin_type, $name = NULL) {
    if (!empty($this->{$plugin_type}['id'])) {
      $name = $this->{$plugin_type}['id'];
    }
    /* @var \Drupal\Core\Plugin\DefaultPluginManager $manager */
    $manager = \Drupal::service('plugin.manager.smbclient.server.' . $plugin_type);
    $this->plugins[$plugin_type] = $manager->createInstance($name, isset($this->{$plugin_type}['configuration']) ? $this->{$plugin_type}['configuration'] : array());
    return $this->plugins[$plugin_type];
  }

}
