<?php

namespace Drupal\smbclient;

use Icewind\SMB\IShare;

/**
 * The share class.
 */
class SmbclientShare implements SmbclientShareInterface {

  /**
   * The share.
   *
   * @var \Icewind\SMB\IShare
   */
  protected $share;

  /**
   * Constructs a class.
   *
   * @param \Icewind\SMB\IShare $share
   */
  public function __construct(IShare $share) {
    $this->share = $share;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($source, $target) {
    return $this->share->get($source, $target);
  }

  /**
   * {@inheritdoc}
   */
  public function putFile($source, $target) {
    return $this->share->put($source, $target);
  }

  /**
   * {@inheritdoc}
   */
  public function readFile($source) {
    return $this->share->read($source);
  }

  /**
   * {@inheritdoc}
   */
  public function writeFile($target) {
    return $this->share->write($target);
  }

  /**
   * {@inheritdoc}
   */
  public function appendFile($target) {
    return $this->share->append($target);
  }

  /**
   * {@inheritdoc}
   */
  public function renameFile($from, $to) {
    return $this->share->rename($from, $to);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFile($path) {
    return $this->share->del($path);
  }

  /**
   * {@inheritdoc}
   */
  public function getDirInfo($path) {
    return $this->share->dir($path);
  }

  /**
   * {@inheritdoc}
   */
  public function getFileInfo($path) {
    return $this->share->stat($path);
  }

  /**
   * {@inheritdoc}
   */
  public function createDir($path) {
    return $this->share->mkdir($path);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDir($path) {
    return $this->share->rmdir($path);
  }

}
