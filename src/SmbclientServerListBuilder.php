<?php

namespace Drupal\smbclient;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of entities.
 */
class SmbclientServerListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smbclient_server_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array();
    $header['label'] = array('data' => t('Title'));
    $header['status'] = array('data' => t('Status'));
    return $header + parent::buildHeader();
  }
  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();

    if ($entity->status()) {
      $row['status']['#markup'] = $this->t('Enabled');
    }
    else {
      $row['status']['#markup'] = $this->t('Disabled');
    }

    $row += parent::buildRow($entity);
    $row['weight']['#delta'] = 50;
    return $row;
  }

}
