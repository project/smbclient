<?php

namespace Drupal\smbclient;

/**
 * Provides an interface for a server.
 */
interface SmbclientServerInterface {

  /**
   * Gets a list of shares.
   *
   * @return \Drupal\smbclient\SmbclientShareInterface[]
   *
   * @throws \Exception
   */
  public function getListShares();

  /**
   * Gets a share.
   *
   * @param string $name
   *
   * @return \Drupal\smbclient\SmbclientShareInterface
   *
   * @throws \Exception
   */
  public function getShare($name);

}
