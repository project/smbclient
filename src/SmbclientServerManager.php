<?php

namespace Drupal\smbclient;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Icewind\SMB\ServerFactory;

/**
 * Managers a SmbclientServer class.
 */
class SmbclientServerManager implements SmbclientServerManagerInterface {

  /**
   * The list of servers.
   *
   * @var \Drupal\smbclient\SmbclientServerInterface[]
   */
  protected $servers;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $storage;

  /**
   * Constructs a class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('smbclient_server');
  }

  /**
   * {@inheritdoc}
   */
  public function getServer($smbclient_server_id) {
    if (!isset($this->servers[$smbclient_server_id])) {
      /** @var \Drupal\smbclient\SmbclientServerEntityInterface|\Drupal\Core\Config\Entity\ConfigEntityBase $entity */
      $entity = $this->storage->load($smbclient_server_id);
      if ($entity && $entity->status()) {
        /** @var \Drupal\smbclient\Plugin\SmbclientServerAuth\SmbclientServerAuthInterface $plugin */
        $plugin = $entity->getPlugin('auth', $entity->getAuthId());
        $factory = new ServerFactory();
        $server = $factory->createServer($entity->getHost(), $plugin->getAuth());
        $this->servers[$smbclient_server_id] = new SmbclientServer($server);
      }
      else {
        $this->servers[$smbclient_server_id] = NULL;
      }
    }
    return $this->servers[$smbclient_server_id];
  }

}
