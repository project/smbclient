<?php

namespace Drupal\smbclient;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a class to check whether a smbclient server is valid and should be deletable.
 */
class SmbclientServerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation === 'delete') {
      return AccessResult::allowedIfHasPermission($account, 'administer smbclient');
    }
    if ($operation === 'update') {
      return AccessResult::allowedIfHasPermission($account, 'administer smbclient');
    }
    return parent::checkAccess($entity, $operation, $account);
  }
}
