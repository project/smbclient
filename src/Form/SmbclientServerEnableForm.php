<?php

namespace Drupal\smbclient\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;


class SmbclientServerEnableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to enable @name smbclient server?', array(
      '@name' => $this->getEntity()->label(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This smbclient server will be executed again.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Enable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->getEntity()->enable()->save();
    $this->messenger()->addMessage($this->t('Enabled smbclient server %name.', array('%name' => $this->getEntity()->label())));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
  
}
