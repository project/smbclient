<?php

namespace Drupal\smbclient\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smbclient\Plugin\SmbclientServerPlugin;

/**
 * Base form controller for smbclient server forms.
 */
class SmbclientServerForm extends EntityForm {

  /**
   * The entity.
   *
   * @var \Drupal\smbclient\SmbclientServerEntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /* @var \Drupal\smbclient\Entity\SmbclientServer $server */
    $server = $this->getEntity();

    $form['title'] = [
      '#title' => t('Title'),
      '#description' => t('This will appear in the administrative interface to easily identify it.'),
      '#type' => 'textfield',
      '#default_value' => $server->getTitle(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $server->id(),
      '#machine_name' => [
        'exists' => '\Drupal\smbclient\Entity\SmbclientServer::load',
        'source' => ['title'],
      ],
      '#disabled' => !$server->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $server->status(),
      '#description' => t('This checkbox enables the smbclient server.'),
    ];

    $form['host'] = [
      '#title' => t('Host'),
      '#description' => t('The host of connection.'),
      '#type' => 'textfield',
      '#default_value' => $server->getHost(),
      '#required' => TRUE,
    ];

    // Setup vertical tabs.
    $form['settings_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    $plugin_types = SmbclientServerPlugin::getPluginTypes();
    foreach ($plugin_types as $plugin_type => $plugin_label) {
      /* @var \Drupal\Core\Plugin\DefaultPluginManager $manager */
      $manager = \Drupal::service('plugin.manager.smbclient.server.' . $plugin_type);
      $plugins = $manager->getDefinitions();

      $plugin_settings = $server->get($plugin_type);

      // Generate select options.
      $options = [];
      foreach ($plugins as $value => $key) {
        if (!empty($key['default']) && $key['default'] == TRUE) {
          $options = [$value => t('@title (Default)', ['@title' => $key['title']])] + $options;
        }
        else {
          $options[$value] = $key['title'];
        }
      }

      $form[$plugin_type] = [
        '#type' => 'details',
        '#title' => $plugin_label,
        '#group' => 'settings_tabs',
        '#tree' => TRUE,
      ];

      $form[$plugin_type]['id'] = [
        '#type' => 'select',
        '#title' => $plugin_label,
        '#options' => $options,
        '#plugin_type' => $plugin_type,
        '#default_value' => $plugin_settings['id'],
        '#description' => $this->t("Select which @plugin to use for this server.", ['@plugin' => $plugin_type]),
        '#group' => 'settings_tabs',
        '#executes_submit_callback' => TRUE,
        '#ajax' => [
          'callback' => [$this, 'updateSelectedPluginType'],
          'wrapper' => $plugin_type . '_settings',
          'method' => 'replace',
        ],
        '#submit' => ['::submitForm', '::rebuild'],
        '#limit_validation_errors' => [[$plugin_type, 'id']],
      ];

      $form[$plugin_type]['select'] = [
        '#type' => 'submit',
        '#name' => $plugin_type . '_select',
        '#value' => t('Select'),
        '#submit' => ['::submitForm', '::rebuild'],
        '#limit_validation_errors' => [[$plugin_type, 'id']],
        '#attributes' => ['class' => ['js-hide']],
      ];

      $plugin = $server->getPlugin($plugin_type);
      $temp_form = [];
      $form[$plugin_type]['configuration'] = $plugin->buildConfigurationForm($temp_form, $form_state);
      $form[$plugin_type]['configuration']['#prefix'] = '<div id="' . $plugin_type . '_settings' . '">';
      $form[$plugin_type]['configuration']['#suffix'] = '</div>';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSelectedPluginType(array $form, FormStateInterface $form_state) {
    return $form[$form_state->getTriggeringElement()['#plugin_type']]['configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function rebuild(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addMessage(t('Server %name has been updated.', ['%name' => $this->getEntity()->label()]));
    $form_state->setRedirectUrl($this->getEntity()->toUrl('collection'));
  }

}
