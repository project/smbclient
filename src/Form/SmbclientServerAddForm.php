<?php

namespace Drupal\smbclient\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add form controller for smbclient server forms.
 */
class SmbclientServerAddForm extends SmbclientServerForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    EntityForm::save($form, $form_state);
    $this->messenger()->addStatus($this->t('Server %name was created.', ['%name' => $this->getEntity()->label()]));
    $form_state->setRedirectUrl($this->getEntity()->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new server');
    return $actions;
  }

}
