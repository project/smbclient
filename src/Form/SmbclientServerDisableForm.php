<?php

namespace Drupal\smbclient\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

class SmbclientServerDisableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to disable @name smbclient server?', array(
      '@name' => $this->getEntity()->label(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This smbclient server will no longer be executed.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Disable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->getEntity()->disable()->save();
    $this->messenger()->addMessage($this->t('Disabled smbclient server %name.', array('%name' => $this->getEntity()->label())));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
  
}
