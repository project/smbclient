<?php

namespace Drupal\smbclient\Plugin\smbclient\SmbclientServerAuth;

use Drupal\smbclient\Plugin\SmbclientServerAuth\SmbclientServerAuthBase;
use Icewind\SMB\AnonymousAuth as IcewindAnonymousAuth;

/**
 * Anonymous auth.
 *
 * @SmbclientServerAuth(
 *   id = "anonymous",
 *   title = @Translation("Anonymous"),
 *   description = @Translation("Provides an anonymous auth."),
 * )
 */
class AnonymousAuth extends SmbclientServerAuthBase {

  /**
   * {@inheritdoc}
   */
  public function getAuth() {
    return new IcewindAnonymousAuth();
  }

}
