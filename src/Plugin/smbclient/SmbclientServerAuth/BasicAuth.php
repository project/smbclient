<?php

namespace Drupal\smbclient\Plugin\smbclient\SmbclientServerAuth;

use Drupal\Core\Form\FormStateInterface;
use Drupal\smbclient\Plugin\SmbclientServerAuth\SmbclientServerAuthBase;
use Icewind\SMB\BasicAuth as IcewindBasicAuth;

/**
 * Basic auth.
 *
 * @SmbclientServerAuth(
 *   id = "basic",
 *   title = @Translation("Basic"),
 *   description = @Translation("Provides a basic auth."),
 * )
 */
class BasicAuth extends SmbclientServerAuthBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
      'workgroup' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->configuration['username'],
      '#fallback' => TRUE,
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $this->configuration['password'],
      '#fallback' => TRUE,
      '#required' => TRUE,
    ];

    $form['workgroup'] = [
      '#type' => 'textfield',
      '#title' => t('Workgroup'),
      '#default_value' => $this->configuration['workgroup'],
      '#fallback' => TRUE,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuth() {
    $settings = $this->getConfiguration();
    return new IcewindBasicAuth($settings['username'], $settings['workgroup'], $settings['password']);
  }

}
