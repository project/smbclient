<?php

namespace Drupal\smbclient\Plugin\SmbclientServerAuth;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * A plugin manager for auth plugins.
 */
class SmbclientServerAuthManager extends DefaultPluginManager {

  /**
   * Constructs a SmbclientServerAuthManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/smbclient/SmbclientServerAuth', $namespaces, $module_handler, '\Drupal\smbclient\Plugin\SmbclientServerAuth\SmbclientServerAuthInterface', 'Drupal\smbclient\Annotation\SmbclientServerAuth');
    $this->alterInfo('smbclient_server_auth_info');
    $this->setCacheBackend($cache_backend, 'smbclient_server_auth');
  }

}
