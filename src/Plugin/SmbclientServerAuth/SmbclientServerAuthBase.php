<?php

namespace Drupal\smbclient\Plugin\SmbclientServerAuth;

use Drupal\smbclient\Plugin\SmbclientServerPlugin;

/**
 * Abstract class for smbclient server auth.
 */
abstract class SmbclientServerAuthBase extends SmbclientServerPlugin implements SmbclientServerAuthInterface {

}
