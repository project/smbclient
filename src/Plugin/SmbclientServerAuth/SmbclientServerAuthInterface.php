<?php

namespace Drupal\smbclient\Plugin\SmbclientServerAuth;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Icewind\SMB\IAuth;

/**
 * Defines an auth method.
 */
interface SmbclientServerAuthInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Gets an IAuth class.
   *
   * @return IAuth
   */
  public function getAuth();

}
