<?php

namespace Drupal\smbclient;

/**
 * Provides an interface for an entity server.
 */
interface SmbclientServerEntityInterface {

  /**
   * Gets the title.
   *
   * @return mixed
   */
  public function getTitle();

  /**
   * Gets the host.
   *
   * @return string
   */
  public function getHost();

  /**
   * Gets the auth id.
   *
   * @return string
   */
  public function getAuthId();

  /**
   * Sets the title.
   *
   * @param $value
   *
   * @return string
   */
  public function setTitle($value);

  /**
   * Sets the host.
   *
   * @param $value
   *
   * @return string
   */
  public function setHost($value);

  /**
   * Gets a server plugin.
   *
   * If no plugin name is provided current plugin of the specified type will
   * be returned.
   *
   * @param string $plugin_type
   *   Name of plugin type.
   * @param string $name
   *   (optional) The name of the plugin.
   *
   * @return \Drupal\smbclient\Plugin\SmbclientServerPlugin
   *   Plugin instance of the specified type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPlugin($plugin_type, $name = NULL);

}
