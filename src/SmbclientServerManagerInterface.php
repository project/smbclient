<?php

namespace Drupal\smbclient;

/**
 * Provides an interface for SmbclientServerManager.
 */
interface SmbclientServerManagerInterface {

  /**
   * Gets a SmbclientServer class.
   *
   * @param string $smbclient_server_id
   *
   * @return \Drupal\smbclient\SmbclientServerInterface
   *
   * @throws \Exception
   */
  public function getServer($smbclient_server_id);

}
