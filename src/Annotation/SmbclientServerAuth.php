<?php

namespace Drupal\smbclient\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an auth plugin annotation object.
 *
 * @Annotation
 */
class SmbclientServerAuth extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable title.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * A short description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

}
