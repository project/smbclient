<?php

namespace Drupal\smbclient;

use Icewind\SMB\IServer;

/**
 * The server class.
 */
class SmbclientServer implements SmbclientServerInterface {

  /**
   * The server.
   *
   * @var \Icewind\SMB\IServer
   */
  protected $server;

  /**
   * Constructs a class.
   *
   * @param \Icewind\SMB\IServer $server
   */
  public function __construct(IServer $server) {
    $this->server = $server;
  }

  /**
   * {@inheritdoc}
   */
  public function getListShares() {
    $shares = $this->server->listShares();
    $shares = array_map(function ($share) {
      return new SmbclientShare($share);
    }, $shares);
    return $shares;
  }

  /**
   * {@inheritdoc}
   */
  public function getShare($name) {
    $share = $this->server->getShare($name);
    return new SmbclientShare($share);
  }

}
